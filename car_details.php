<?php 
include("inc/header.php");
include("db/db.php");
$id=$_GET['id'];

$sql = "SELECT * FROM car where CarId='$id'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
?>
<!-- Page banner start-->
<!-- <div class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="breadcrumb-area">
                    <h2><?php echo $row["CarName"]; ?></h2>
                    <div class="line-dec"></div>
                    <h5><?php echo $row["CarDescription"]; ?></h5>
                    <p>
                        <a href="index.php" class="home-btn">Home</a>
                        <a href="car_details.php" class="active-page">Car Details</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
 -->
<!-- Car details start-->
<div class="car-details content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="option-bar">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="section-heading">
                                <i class="fa fa-car"></i>
                                <h2><?php echo $row["CarName"]; ?></h2>
                                <div class="border"></div>
                                <h4>Details of <?php echo $row["CarName"]; ?></h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 text-right">
                            <div class="car-details-header-price">
                                <h3>$<?php echo $row["CarPrice"]; ?></h3>
                                <p>
                                   <?php
                                 if($row["Rating"]==1){
                                    echo '<i class="fa fa-star orange-color"></i>';
                                 }else if($row["Rating"]==2){
                                     echo '<i class="fa fa-star orange-color"></i>';
                                     echo '<i class="fa fa-star orange-color"></i>';
                                 }else if($row["Rating"]==3){
                                     echo '<i class="fa fa-star orange-color"></i>';
                                     echo '<i class="fa fa-star orange-color"></i>';
                                     echo '<i class="fa fa-star orange-color"></i>';
                                 }else if($row["Rating"]==4){
                                     echo '<i class="fa fa-star orange-color"></i>';
                                     echo '<i class="fa fa-star orange-color"></i>';
                                     echo '<i class="fa fa-star orange-color"></i>';
                                     echo '<i class="fa fa-star orange-color"></i>';
                                 }else if($row["Rating"]==5){
                                     echo '<i class="fa fa-star orange-color"></i>';
                                     echo '<i class="fa fa-star orange-color"></i>';
                                     echo '<i class="fa fa-star orange-color"></i>';
                                     echo '<i class="fa fa-star orange-color"></i>';
                                     echo '<i class="fa fa-star orange-color"></i>';
                                 }


                                ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <!-- Car detail slider start-->
                <div class="car-detail-slider">
                    <div id='carousel-custom' class='carousel slide' data-ride='carousel'>
                        <div class='carousel-outer'>
                            <!-- Wrapper for slides -->
                             
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="http://admin.lnbmotorsltd.co.nz/images/<?php echo $row["CarImageUrl"]; ?>" class="thumb-preview" alt="<?php echo $row["CarName"]; ?>"/>
                                </div>
                            	<?php
                            	$sql1 = "SELECT * FROM carimage where CarId='$id'";
								$result1 = $conn->query($sql1);
								while($row1 = $result1->fetch_assoc()){


                            	?>
                                <div class="item">
                                    <img src="http://admin.lnbmotorsltd.co.nz/images/<?php echo $row1["ImageUrl"]; ?>" class="thumb-preview" alt="<?php echo $row["CarName"]; ?>"/>
                                </div>
                                <?php
                            	}
                                ?>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-custom" role="button" data-slide="prev">
                              <span class="slider-mover-left no-bg" aria-hidden="true">
                                 <img src="img/left-chevron.png" alt="left-chevron">
                              </span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-custom" role="button" data-slide="next">
                                <span class="slider-mover-right no-bg" aria-hidden="true">
                                    <img src="img/right-chevron.png" alt="right-chevron">
                                </span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                        <!-- Indicators -->
                        <ol class="carousel-indicators thumbs visible-lg visible-md">
                        	<?php
                            	
                                $i=0;
								while($row2 = $result1->fetch_assoc()){


                            	?>
                            <li data-target="#carousel-custom" data-slide-to="<?php echo $i; ?>" class="active">
                                <img src="http://admin.lnbmotorsltd.co.nz/images/<?php echo $row2["ImageUrl"]; ?>" alt="Chevrolet Impala"/>
                            </li>
                            <?php
                            $i=$i+1;
                            }
                            ?>
                            
                        </ol>
                    </div>
                </div>
                <!-- Car detail slider End-->

                <div class="clearfix"></div>
                
                <!-- Car details content body start -->
                <div class="car-details-content-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1default" data-toggle="tab" aria-expanded="true">Vehicle Overview</a></li>
                        <li class=""><a href="#tab2default" data-toggle="tab" aria-expanded="false">Features &amp; Options</a></li>
                        <li class=""><a href="#tab3default" data-toggle="tab" aria-expanded="false">Technical Specifications</a></li>
                        
                    </ul>
                    <div class="panel with-nav-tabs panel-default">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab1default">
                                    <p><?php echo $row["Overview"]; ?></p>
                                    
                                </div>
                                <div class="tab-pane fade features" id="tab2default">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                        <ul>
                                            <?php
                                            $sql2 = "SELECT * FROM features where CarId='$id'";
                                            $result2 = $conn->query($sql2);
                                            while($row2 = $result2->fetch_assoc()){


                                            ?>
                                            <li>
                                               <i class="fa fa-check"></i>
                                              <?php echo $row2["FeaturesName"]; ?>
                                            </li>
                                            <?php }?>
                                            
                                         </ul>
                                    </div>
                                    
                                  
                                    </div>
                                </div>
                                <div class="tab-pane fade technical" id="tab3default">
                                    <ul>
                                         <?php
                                            $sql3 = "SELECT * FROM technical where CarId='$id'";
                                            $result3 = $conn->query($sql3);
                                            $row3 = $result3->fetch_assoc();


                                            ?>
                                        <li>
                                            Layout / number of cylinders <span> <?php echo $row3["Layout"]; ?></span>
                                        </li>
                                         <li>
                                            Displacement <span><?php echo $row3["Displacement"]; ?></span>
                                        </li>
                                        <li>
                                            Engine Layout <span> <?php echo $row3["Engine"]; ?></span>
                                        </li>
                                        <li>
                                            Horespower <span> <?php echo $row3["Horespower"]; ?></span>
                                        </li>
                                        <li>
                                            @ rpm <span> <?php echo $row3["Rpm"]; ?></span>
                                        </li>
                                        <li>
                                            Torque <span> <?php echo $row3["Torque"]; ?></span>
                                        </li>
                                        <li>
                                            Compression ratio <span> <?php echo $row3["Compression"]; ?></span>
                                        </li>
                                        
                                    </ul> 
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Car details content body end -->

               
                <div class="clearfix"></div>

                
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <!-- Car sidebar right Start-->
                <div class="car-sidebar-right">
                    <!-- Car detail block Start-->
                    <div class="car-detail-block mrg-b-30">
                        <div class="section-heading">
                            <i class="fa fa-search-plus"></i>
                            <h2>Car specifications</h2>
                            <div class="border"></div>
                            <h4>Check the car specifications</h4>
                        </div>
                        <h2 class="title">Detials</h2>
                        <ul class="car-detail-info-list">
                            <li>
                                <span>Engine:</span><?php echo $row["Engine"]; ?>
                            </li>
                             <li>
                                <span>Mileage:</span><?php echo $row["Mileage"]; ?>
                            </li>
                            <li>
                                <span>Model:</span><?php echo $row["Model"]; ?>
                            </li>
                            <li>
                                <span>Transmissione:</span><?php echo $row["Transmissione"]; ?>
                            </li>
                            <li>
                                <span>Color:</span><?php echo $row["Color"]; ?>
                            </li>
                            <li>
                                <span>Passengers:</span><?php echo $row["Passengers"]; ?>
                            </li>
                            
                            
                        </ul>
                    </div>
                    <!-- Car detail block end-->

                    <div class="clearfix"></div>

                    <!-- Dealer contact Start-->
                    <div class="dealer-contact mrg-b-30">
                        <h2 class="title">Helping Center</h2>
                        <ul>
                            <li>
                                <span>Contact:</span> Person: L&B Motors
                            </li>
                            <li>
                                <span>Address:</span> 29 Grey Road,Timaru Central,Timaru 7910
                            </li>
                            <li>
                                <span>Mobile 1:</span> <a href="tel:+0226233799">+0226-2337 99</a>
                            </li>
                            <li>
                                <span>Mobile 2:</span> <a href="tel:+0226-959778">+0226-9597 78</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Dealer contact End-->

                    <div class="clearfix"></div>

                    <!-- Share Start-->
                    <div class="share mrg-b-30">
                        <h2>Share</h2>
                        <div class="clearfix"></div>
                        <ul class="social-list">
                            <li>
                                <a href="#" class="facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                           
                            <li>
                                <a href="#" class="google">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                           
                           
                        </ul>
                   </div>
                    <!-- Share End-->

                    <div class="clearfix"></div>
                    
                   
                </div>
                <!-- Car sidebar right end-->
            </div>
        </div>
    </div>
</div>
<!-- Car details start-->
<!-- Page banner end -->
<?php include("inc/footer.php") ?>