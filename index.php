<?php include("inc/header.php") ?>
<!-- banner start-->
<div class="banner">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
<!-- Wrapper for slides -->
<div class="carousel-inner" role="listbox">
<div class="item active item-1">
    <div class="container">
        <div class="banner-slider-inner-1">
            <h1>Low price <span> Good Quality Cars</span> </h1>
            
        </div>
    </div>
</div>
<div class="item item-2">
    <div class="container">
        <div class="banner-slider-inner-1">
            <h1>We provide   <span>finance &</span></h1>
            <h2>mechanical warranty </h2>
            
        </div>
    </div>
</div>

</div>
<!-- Controls -->
<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
<span class="slider-mover-left" aria-hidden="true">
    <img src="img/left-chevron.png" alt="left-chevron">
</span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
<span class="slider-mover-right" aria-hidden="true">
    <img src="img/right-chevron.png" alt="right-chevron">
</span>
<span class="sr-only">Next</span>
</a>
</div>
</div>
<!-- banner end-->
<!-- Recent car start-->
<div class="recent-car content-area">
<div class="container">
<div class="recent-car-content">
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="section-heading">
            <i class="fa fa-car"></i>
            <h2>Recent cars</h2>
            <div class="border"></div>
            <h4>Check our recent motors</h4>
        </div>
    </div>
</div>
<div class="row">
    <?php
    $i=1;
    include("db/db.php");
    $sql = "SELECT * FROM car where CarStatus=0 order by CarId desc limit 10";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
    if($i%3==0){
    ?>
    
    
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="thumbnail car-box">
            <?php if($row["CarStatus"]==1){ ?>
            <a href="#" class="sale">
                <span>Sold</span>
            </a>
            <?php } ?>
            <?php
            if($row["CarImageUrl"]==""){
            echo '<img src="img/noimage.png" />';
            }else{
            echo '<img src="http://admin.lnbmotorsltd.co.nz/images/'.$row["CarImageUrl"].'"  style="
            height: 250px !important;
            "/>';
            }
            ?>
            
            <div class="caption car-content">
                <div class="header b-items-cars-one-info-header s-lineDownLeft">
                    <h3>
                    <a href="car_details.php?id=<?php echo $row["CarId"] ?>"><?php echo $row["CarName"] ?></a>
                    <span>$<?php echo $row["CarPrice"] ?></span>
                    </h3>
                </div>
                <p><?php echo $row["CarDescription"] ?></p>
                <div class="car-tags">
                    <ul>
                        <li><?php echo $row["Model"] ?></li>
                        <li><?php echo $row["Engine"] ?></li>
                        <li><?php echo $row["Color"] ?></li>
                        <li><?php echo $row["Mileage"] ?></li>
                    </ul>
                </div>
                <div class="ster-fa">
                    <?php
                    if($row["Rating"]==1){
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==2){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==3){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==4){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==5){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }
                    ?>
                    
                </div>
                <a href="car_details.php?id=<?php echo $row["CarId"] ?>" class="btn details-button">Deatils</a>
            </div>
        </div>
    </div>
    
</div>
<div class="row">
    <?php
    }else{
    ?>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="thumbnail car-box">
            <?php if($row["CarStatus"]==1){ ?>
            <a href="#" class="sale">
                <span>Sold</span>
            </a>
            <?php } ?>
            <?php
            if($row["CarImageUrl"]==""){
            echo '<img src="img/noimage.png" />';
            }else{
            echo '<img src="http://admin.lnbmotorsltd.co.nz/images/'.$row["CarImageUrl"].'" style="
            height: 250px !important;
            " />';
            }
            ?>
            
            <div class="caption car-content">
                <div class="header b-items-cars-one-info-header s-lineDownLeft">
                    <h3>
                    <a href="car_details.php?id=<?php echo $row["CarId"] ?>"><?php echo $row["CarName"] ?></a>
                    <span>$<?php echo $row["CarPrice"] ?></span>
                    </h3>
                </div>
                <p><?php echo $row["CarDescription"] ?></p>
                <div class="car-tags">
                    <ul>
                        <li><?php echo $row["Model"] ?></li>
                        <li><?php echo $row["Engine"] ?></li>
                        <li><?php echo $row["Color"] ?></li>
                        <li><?php echo $row["Mileage"] ?></li>
                    </ul>
                </div>
                <div class="ster-fa">
                    <?php
                    if($row["Rating"]==1){
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==2){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==3){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==4){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==5){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }
                    ?>
                    
                </div>
                <a href="car_details.php?id=<?php echo $row["CarId"] ?>" class="btn details-button">Deatils</a>
            </div>
        </div>
    </div>
    <?php
    }
    $i=$i+1;
    }
    ?>
</div>
</div>
</div>
<?php include("inc/footer.php") ?>