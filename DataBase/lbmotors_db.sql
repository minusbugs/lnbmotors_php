-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 182.50.133.90:3306
-- Generation Time: Apr 03, 2018 at 08:19 PM
-- Server version: 5.5.43-37.2-log
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lbmotors_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `CarId` int(10) NOT NULL,
  `CarName` varchar(200) NOT NULL,
  `CarDescription` text NOT NULL,
  `CarPrice` double NOT NULL,
  `Engine` varchar(100) NOT NULL,
  `Mileage` varchar(100) NOT NULL,
  `Model` int(10) NOT NULL,
  `Transmissione` varchar(30) NOT NULL,
  `Color` varchar(50) NOT NULL,
  `Rating` int(10) NOT NULL,
  `Passengers` int(10) NOT NULL,
  `Overview` text NOT NULL,
  `Features` text NOT NULL,
  `CarImageUrl` varchar(200) NOT NULL,
  `CreatedDate` varchar(10) NOT NULL,
  `UpdateDate` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`CarId`, `CarName`, `CarDescription`, `CarPrice`, `Engine`, `Mileage`, `Model`, `Transmissione`, `Color`, `Rating`, `Passengers`, `Overview`, `Features`, `CarImageUrl`, `CreatedDate`, `UpdateDate`) VALUES
(1, 'VENCER SARTHE SUPERCAR', 'Next level Pinterest farm-to-table selvage gentrify street art raw denim Helvetica street art pork belly.', 54905, 'DOHC 24-valve V-6', '35.000 KM', 2014, '6 Speed Auto', 'White', 3, 4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem vulputate interdum et vel eros. Maecenas eros enim, tincidunt vel turpis vel, dapibus tempus nulla. Donec vel nulla dui. Pellentesque sed ante sed ligula hendrerit condimentum. Suspendisse rhoncus fringilla ipsum quis porta. Morbi tincidunt viverra pharetra.\r\n\r\nVestibulum vel mauris et odio lobortis laoreet eget eu magna. Proin mauris erat, luctus at nulla ut, lobortis mattis magna. Morbi a arcu lacus. Maecenas tristique velit vitae nisi consectetur, in mattis diam sodales. Mauris sagittis sem mattis justo bibendum, a eleifend dolor facilisis. Mauris nec pharetra tortor, ac aliquam felis. Nunc pretium erat sed quam consectetur fringilla.\r\n\r\nAliquam ultricies nunc porta metus interdum mollis. Donec porttitor libero augue, vehicula tincidunt lectus placerat a. Sed tincidunt dolor non sem dictum dignissim. Nulla vulputate orci felis, ac ornare purus ultricies a. Fusce euismod magna orci, sit amet aliquam turpis dignissim ac. In at tortor at ligula pharetra sollicitudin. Sed tincidunt, purus eget laoreet elementum, felis est pharetra ante, tincidunt feugiat libero enim sed risus.\r\n\r\nSed at leo sit amet mi bibendum aliquam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent cursus varius odio, non faucibus dui. Nunc vehicula lectus sed velit suscipit aliquam vitae eu ipsum. Curabitur hendrerit magna a quam semper, at tristique mauris gravida. Donec consequat elementum lorem, ac luctus ligula. Quisque viverra fringilla mi vel aliquam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', ' Adaptive Cruise Control\r\n Airbags\r\n Air Conditioning\r\n Alarm System\r\n Anti-theft Protection\r\n Audio Interface\r\n Automatic Climate Control\r\n Automatic Headlights\r\n Auto Start/Stop\r\n Bi-Xenon Headlights\r\n Audio Interface\r\n Bluetooth? Handset\r\n BOSE? Surround Sound\r\n Burmester? Surround Sound\r\n CD/DVD Autochanger\r\n CDR Audio\r\n Cruise Control\r\n Direct Fuel Injection\r\n Electric Parking Brake\r\n Floor Mats\r\n Garage Door Opener\r\n Leather Package\r\n Locking Rear Differential\r\n Luggage Compartments\r\n Manual Transmission\r\n Navigation Module\r\n Online Services\r\n ParkAssist\r\n Porsche Communication\r\n CD/DVD Autochanger\r\n Power Steering\r\n Reversing Camera\r\n Roll-over Protection\r\n Seat Heating\r\n Seat Ventilation\r\n Sound Package Plus\r\n Sport Chrono Package\r\n Steering Wheel Heating\r\n Tire Pressure Monitoring\r\n Universal Audio Interface\r\n Voice Control System\r\n Wind Deflector', '1.jpg', '', ''),
(2, 'AUDI Q7 2017', 'Next level Pinterest farm-to-table selvage gentrify street art raw denim Helvetica street art pork belly.', 54905, 'DOHC 24-valve V-6', '35.000 KM', 2000, '6 Speed Auto', 'White', 5, 4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem vulputate interdum et vel eros. Maecenas eros enim, tincidunt vel turpis vel, dapibus tempus nulla. Donec vel nulla dui. Pellentesque sed ante sed ligula hendrerit condimentum. Suspendisse rhoncus fringilla ipsum quis porta. Morbi tincidunt viverra pharetra', ' Adaptive Cruise Control\r\n ', '4.jpg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `carimage`
--

CREATE TABLE `carimage` (
  `ImageId` int(10) NOT NULL,
  `CarId` int(10) NOT NULL,
  `ImageUrl` varchar(200) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carimage`
--

INSERT INTO `carimage` (`ImageId`, `CarId`, `ImageUrl`, `CreatedOn`) VALUES
(1, 1, '1.jpg', '0000-00-00 00:00:00'),
(2, 1, '2.jpg', '0000-00-00 00:00:00'),
(3, 1, '3.jpg', '2018-04-04 11:09:14'),
(4, 2, '4.jpg', '2018-04-03 16:38:45'),
(5, 2, '5.jpg', '2018-04-03 16:39:27'),
(6, 2, '6.jpg', '2018-04-03 16:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `FeaturesId` int(10) NOT NULL,
  `CarId` int(10) NOT NULL,
  `FeaturesName` varchar(100) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`FeaturesId`, `CarId`, `FeaturesName`, `CreatedOn`) VALUES
(1, 1, 'Adaptive Cruise Control', '2018-04-03 17:58:28'),
(2, 1, 'Airbags', '2018-04-03 17:58:28'),
(3, 2, 'Air Conditioning', '2018-04-03 17:58:45'),
(4, 2, 'Airbags', '2018-04-03 17:58:45');

-- --------------------------------------------------------

--
-- Table structure for table `technical`
--

CREATE TABLE `technical` (
  `TechnicalId` int(10) NOT NULL,
  `CarId` int(10) NOT NULL,
  `Layout` int(10) NOT NULL,
  `Displacement` varchar(100) NOT NULL,
  `Engine` varchar(100) NOT NULL,
  `Horespower` varchar(100) NOT NULL,
  `Rpm` varchar(100) NOT NULL,
  `Torque` varchar(100) NOT NULL,
  `Compression` varchar(100) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `technical`
--

INSERT INTO `technical` (`TechnicalId`, `CarId`, `Layout`, `Displacement`, `Engine`, `Horespower`, `Rpm`, `Torque`, `Compression`, `CreatedDate`) VALUES
(1, 1, 6, '3.4 l', 'Mid-engine', '315 hp', '6,800 rpm', '266 lb.-ft', '12.5 : 1', '2018-04-03 18:08:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`CarId`);

--
-- Indexes for table `carimage`
--
ALTER TABLE `carimage`
  ADD PRIMARY KEY (`ImageId`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`FeaturesId`);

--
-- Indexes for table `technical`
--
ALTER TABLE `technical`
  ADD PRIMARY KEY (`TechnicalId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `car`
--
ALTER TABLE `car`
  MODIFY `CarId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `carimage`
--
ALTER TABLE `carimage`
  MODIFY `ImageId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `FeaturesId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `technical`
--
ALTER TABLE `technical`
  MODIFY `TechnicalId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
