<!DOCTYPE html>
<html lang="en">
<head>
    <title>L&B Motors PVT LTD</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">


    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/slider.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome-4.5.0/css/font-awesome.min.css">
     <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Oleo+Script:400,700" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Top header start -->
<header class="top-header hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <ul class="list-inline">
                    <li>
                        <a href="tel:+0226233799"><i class="fa fa-phone pr-5 pl-10"></i>  +0226-2337 99</a>
                    </li>
                    <li>
                        <a href="tel:+0226-9597 78"><i class="fa fa-phone pr-5 pl-10"></i>  +0226-9597 78</a>
                    </li>
                    <li>
                        <a href="mailto:lnbmotorstimaru@gmail.com">
                            <i class="fa fa-envelope-o pr-5 pl-10"></i>lnbmotorstimaru@gmail.com
                        </a>
                    </li>
                </ul>
            </div>
           
        </div>
    </div>
</header>
<!-- Top header end -->
<!-- Main header start-->
<div class="main-header">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand-logo" href="index.php">
                    <img src="img/logo.png" alt="CAR HOUSE">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li >
                        <a href="index.php?page=hom" class="<?php echo $_GET["page"]=='hom'? 'active':'' ?>" >
                        Home
                        </a>
                    </li>
                    <li>
                        <a href="CarListing.php?page=acars" class="<?php echo $_GET["page"]=='acars'? 'active':'' ?>">
                            Available Car's
                        </a>
                    </li>
                    <li>
                        <a href="SoldCar.php?page=scars" class="<?php echo $_GET["page"]=='scars'? 'active':'' ?>">
                            Sold Car's
                        </a>
                    </li>
                    <li>
                        <a href="about.php?page=abt" class="<?php echo $_GET["page"]=='abt'? 'active':'' ?>">
                            About Us
                        </a>
                    </li>
                    <li>
                        <a href="contact.php?page=cnt" class="<?php echo $_GET["page"]=='cnt'? 'active':'' ?>">
                            Contact Us
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
            <!-- /.container -->
        </nav>
    </div>
</div>
<!-- Main header end-->