

<!-- Sub footer start-->
<div class="sub-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <p>Copyright 2016. All rights reserved by:<span> L&B Motors</span></p>
            </div>
             <div class="col-md-3 col-sm-4">
                <p>Design & Developed by <a href="http://minusbugs.com">minusbugs</a></p>
            </div>
            <div class="col-md-5 col-sm-4 hidden-xs ">
                <ul>
                    <li>
                        <a href="contact.php">Contact Us</a>
                    </li>
                    <li>
                        <a href="about.php">About Us</a>
                    </li>
                     <li>
                        <a href="SoldCar.php">Sold Car's</a>
                    </li>
                     <li>
                        <a href="CarListing.php">Car Listing</a>
                    </li>


                    <li>
                        <a href="index.php">Home</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Sub footer end-->

<script src="js/jquery-2.2.0.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-slider.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script src="js/app.js"></script>


</body>
</html>