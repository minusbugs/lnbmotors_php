<?php 
include("inc/header.php");
include("db/db.php");

$sql = "SELECT * FROM car where CarStatus=1 order by CarId desc";
$result = $conn->query($sql);
?>


<!-- Content area start-->
<div class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<div class="option-bar">
                   <div class="row">
                       <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="section-heading ">
                               <i class="fa fa-car"></i>
                               <h2>All Sold Cars</h2>
                               <div class="border"></div>
                               <h4>Check our all motors</h4>
                           </div>
                       </div>
                       
                   </div>
               </div> 
             <div class="row">
    <?php
    $i=1;
    include("db/db.php");
    $sql = "SELECT * FROM car where CarStatus=1 order by CarId desc limit 10";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
    if($i%3==0){
    ?>
    
    
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="thumbnail car-box">
            <?php if($row["CarStatus"]==1){ ?>
            <a href="#" class="sale">
                <span>Sold</span>
            </a>
            <?php } ?>
            <?php
            if($row["CarImageUrl"]==""){
            echo '<img src="img/noimage.png" />';
            }else{
            echo '<img src="http://admin.lnbmotorsltd.co.nz/images/'.$row["CarImageUrl"].'"  style="
            height: 250px !important;
            "/>';
            }
            ?>
            
            <div class="caption car-content">
                <div class="header b-items-cars-one-info-header s-lineDownLeft">
                    <h3>
                    <a href="car_details.php?id=<?php echo $row["CarId"] ?>"><?php echo $row["CarName"] ?></a>
                    <span>$<?php echo $row["CarPrice"] ?></span>
                    </h3>
                </div>
                <p><?php echo $row["CarDescription"] ?></p>
                <div class="car-tags">
                    <ul>
                        <li><?php echo $row["Model"] ?></li>
                        <li><?php echo $row["Engine"] ?></li>
                        <li><?php echo $row["Color"] ?></li>
                        <li><?php echo $row["Mileage"] ?></li>
                    </ul>
                </div>
                <div class="ster-fa">
                    <?php
                    if($row["Rating"]==1){
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==2){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==3){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==4){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==5){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }
                    ?>
                    
                </div>
                <a href="car_details.php?id=<?php echo $row["CarId"] ?>" class="btn details-button">Deatils</a>
            </div>
        </div>
    </div>
    
</div>
<div class="row">
    <?php
    }else{
    ?>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="thumbnail car-box">
            <?php if($row["CarStatus"]==1){ ?>
            <a href="#" class="sale">
                <span>Sold</span>
            </a>
            <?php } ?>
            <?php
            if($row["CarImageUrl"]==""){
            echo '<img src="img/noimage.png" />';
            }else{
            echo '<img src="http://admin.lnbmotorsltd.co.nz/images/'.$row["CarImageUrl"].'" style="
            height: 250px !important;
            " />';
            }
            ?>
            
            <div class="caption car-content">
                <div class="header b-items-cars-one-info-header s-lineDownLeft">
                    <h3>
                    <a href="car_details.php?id=<?php echo $row["CarId"] ?>"><?php echo $row["CarName"] ?></a>
                    <span>$<?php echo $row["CarPrice"] ?></span>
                    </h3>
                </div>
                <p><?php echo $row["CarDescription"] ?></p>
                <div class="car-tags">
                    <ul>
                        <li><?php echo $row["Model"] ?></li>
                        <li><?php echo $row["Engine"] ?></li>
                        <li><?php echo $row["Color"] ?></li>
                        <li><?php echo $row["Mileage"] ?></li>
                    </ul>
                </div>
                <div class="ster-fa">
                    <?php
                    if($row["Rating"]==1){
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==2){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==3){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==4){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }else if($row["Rating"]==5){
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    echo '<i class="fa fa-star orange-color"></i>';
                    }
                    ?>
                    
                </div>
                <a href="car_details.php?id=<?php echo $row["CarId"] ?>" class="btn details-button">Deatils</a>
            </div>
        </div>
    </div>
    <?php
    }
    $i=$i+1;
    }
    ?>
</div>

            </div>
        </div>
    </div>
</div>
<?php include("inc/footer.php") ?>