<?php  include("inc/header.php"); ?>
<!-- page banner start-->
<div class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="breadcrumb-area">
                    <h2>About L&B Motor's</h2>
                    <div class="line-dec"></div>
                    <h5>Praesent volutpat nisi sed imperdiet facilisis felis turpis fermentum lectus</h5>
                    <p>
                        <a href="index.php" class="home-btn">Home</a>
                        <a href="about.php" class="active-page">About us</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- page banner end -->
<!-- About body start-->
<div class="about-body">
    <!-- Page section start-->
    <div class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h2 class="title">Welcome to the L&B Motors</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur corporis lab. Architecto, maiores, similique, eos, ea doloribus nihil accusantium fuga numquam corporis nostrum eligendi nemo sapiente quibusdam consequuntur tempore. Suscipit.</p>

                    <!-- Icon list -->
                    <ul class="icon-list">
                        <li>
                            <i class="fa fa-check"></i>
                            Volutpat luctus lacus hendrerit taciti lobortis in.
                        </li>
                        <li>
                            <i class="fa fa-check"></i>
                            Faucibus amet consectetur erat donec venenatis leo.
                        </li>
                        <li>
                            <i class="fa fa-check"></i>
                            Ullamcorper conubia, aenean quisque mattis malesuada.
                        </li>
                        <li>
                            <i class="fa fa-check"></i>
                            quam quisque ligula, maecenas ligula proin ut augue.
                        </li>
                    </ul>

                    <a href="contact.html" class="btn btn-contact">Contact us</a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="car-video-frame">
                        <iframe src="https://www.youtube.com/embed/21CY9RtMQkU" ></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page section end-->

    <!-- Page Section start-->
    <section class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h2 class="title">Why Choose Us</h2>
                    <p>Curabitur libero. Donec facilisis velit eu est. Phasellus cons quat. Aenean vitae quam. Vivamus etyd nunc. Nunc consequsem velde metus imperdiet lacinia. Lorem ipsum dolor sit amet sed consectetur adipisicing elit sed do eiusmod.</p>
                    <!-- Icon list -->
                    <ul class="icon-list">
                        <li>
                            <i class="fa fa-check"></i>
                            Donec facilisis velit eu est phasellus consequat quis nostrud
                        </li>
                        <li>
                            <i class="fa fa-check"></i>
                            Aenean vitae quam. Vivamus et nunc nunc conseq
                        </li>
                        <li>
                            <i class="fa fa-check"></i>
                            Sem vel metus imperdiet lacinia enean
                        </li>
                        <li>
                            <i class="fa fa-check"></i>
                            Dapibus aliquam augue fusce eleifend quisque tels
                        </li>
                        <li>
                            <i class="fa fa-check"></i>
                            Lorem ipsum dolor sit amet, consectetur
                        </li>
                        <li>
                            <i class="fa fa-check"></i>
                            Adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore Magna
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h2 class="title">More Info</h2>
                    
                    <div class="panel-div">
                        <div class="panel-group" id="accordion" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <i class="fa fa-plus"></i>Fair Price for Everyone
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                                    <div class="panel-body">
                                        <p>Curabitur libero. Donec facilisis velit est. Phasellus consquat. Aenean vitae quam. Vivam etl nunc. Nunc con sequsem velde metus imperdiet lacinia. Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <i class="fa fa-plus"></i>Large Number of Vehicles
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                   <div class="panel-body">
                                        <p>Curabitur libero. Donec facilisis velit est. Phasellus consquat. Aenean vitae quam. Vivam etl nunc. Nunc con sequsem velde metus imperdiet lacinia. Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingfive">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                            <i class="fa fa-plus"></i>Auto Loan Available
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive">
                                    <div class="panel-body">
                                        <p>Curabitur libero. Donec facilisis velit est. Phasellus consquat. Aenean vitae quam. Vivam etl nunc. Nunc con sequsem velde metus imperdiet lacinia. Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </section>
    <!-- Page Section end-->
    
    <!-- About team meet start-->
    <section class="about-team-meet"></section>
    <!-- About team meet end-->
</div>
<?php  include("inc/footer.php"); ?>