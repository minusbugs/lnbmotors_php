<?php  include("inc/header.php"); ?>
<!-- Page banner start-->
<div class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="breadcrumb-area">
                    <h2>Contact Us</h2>
                    <div class="line-dec"></div>
                    <h5>Contact for our support</h5>
                    <p>
                        <a href="index.php" class="home-btn">Home</a>
                        <a href="contact.php" class="active-page">Contact Us</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page banner end -->
<div class="contact-us-body">
	 <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-xs-12">
                <h2 class="title">Contact Form</h2>
                <div class="contact-form">
                    <form id="contact_form" action="index.php" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group full-name">
                                    <input type="text" class="input-text" name="full-name" id="full-name" placeholder="Full Name">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group email address">
                                    <input type="text" class="input-text" name="email" id="email" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group Phone Number"> 
                                    <input type="text" class="input-text" name="phone-number" id="phone-number" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group your website">
                                    <input type="text" class="input-text" name="yor-website" id="your-website" placeholder="Your Website">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group message">
                                    <textarea id="message" class="input-text" name="message" placeholder="Write message"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mrg-btnn">
                                <input type="submit" name="sent message" class=" btn btn-message" value="send message">
                            </div>
                        </div>
                    </form>     
                </div>
            </div>

            <div class="col-lg-4 col-md-4  col-xs-12">
                <!-- contact details start-->
                <div class="contact-details">
                    <div class="item">
                        <div class="icon">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="content">
                            <h5>Address</h5>
                            <p>29 Grey Road,Timaru Central,Timaru 7910 </p>

                        </div>
                    </div>

                    <div class="item">
                        <div class="icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="content">
                            <h5>Phone</h5>
                            <p><span>office:</span> <a href="tel:+0226233799">+0226-2337 99</a></p>
                            <p><span>Mobile:</span> <a href="tel:+0226959778">+0226-9597 78 </a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="icon">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="content">
                            <h5>Email</h5>
                            <p>
                               <span>office:</span><a href="mailto:sales@carhouse.com"> lnbmotorstimaru@gmail.com</a>
                            </p>

                            <p>
                               <span>Mobile:</span><a href="tel:+0226233799"> 0226-2337 99</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- contact details end-->
                
                <!-- share start-->
                <div class="share">
                    <h2>Share</h2>
                    <ul class="social-list">
                        <li>
                            <a href="#" class="facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                       
                        <li>
                            <a href="#" class="google">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                       
                    </ul>
                </div>
                <!-- share end-->
            </div>
        </div>
    </div>

</div>
<?php  include("inc/footer.php"); ?>















   